package com.shipserv.takehomeexam.service;

import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.mapper.CompanyMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyServiceIntegTest {

    @Autowired
    private CompanyService sut;

    @MockBean
    private CompanyMapper companyMapper;

    @Test
    public void testGetAllExistingNullParams() {
        List<CompanyDTO> actual = sut.findAll(null, null, null, null, null);
        Assert.assertNotNull(actual);
    }

    @Test(expected = APIException.class)
    public void testGetAllExistingNotNullParams() {
        List<CompanyDTO> actual = sut.findAll("ANY", "ANY", "BUYER", "YES", "YES");
    }
}
