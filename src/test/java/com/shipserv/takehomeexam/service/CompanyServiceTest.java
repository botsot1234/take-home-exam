package com.shipserv.takehomeexam.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.mapper.CompanyMapper;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.repository.CompanyRepository;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CompanyServiceTest {

    @InjectMocks
    private CompanyService sut;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private CompanyMapper companyMapper;

    @Test
    public void testSoftDelete() {
        when(companyRepository.findById(anyLong())).thenReturn(Optional.of(new Company()));
        when(companyRepository.save(any(Company.class))).thenReturn(Company.builder().build());
        when(companyMapper.mapFromEntityToDTO(any(Company.class)))
            .thenReturn(CompanyDTO.builder()
                .deleted(Flag.YES.getValue())
                .build());
        CompanyDTO actual = sut.softDelete("123");
        assertEquals(Flag.YES.getValue(), actual.getDeleted());
    }


    @Test(expected = APIException.class)
    public void testCreateWhenIllegalArgThenThrowException() {
        when(companyMapper.mapFromDTOToEntity(Mockito.any(CompanyDTO.class)))
            .thenThrow(IllegalArgumentException.class);
        CompanyDTO actual = sut.create(CompanyDTO.builder().build());
    }

    @Test
    public void testUpdate() {
        when(companyRepository.findById(anyLong())).thenReturn(Optional.of(new Company()));
        when(companyMapper.mapFromExisting(Mockito.any(Company.class), Mockito.any(CompanyDTO.class)))
            .thenReturn(new Company());
        when(companyRepository.save(Mockito.any(Company.class)))
            .thenReturn(new Company());
        when(companyMapper.mapFromEntityToDTO(Mockito.any(Company.class)))
            .thenReturn(CompanyDTO.builder().id(Long.valueOf(123)).build());
        CompanyDTO actual = sut.update(CompanyDTO.builder().id(Long.valueOf(123)).build());
        Assert.assertEquals(Long.valueOf(123), actual.getId());
    }

    @Test
    public void testCreateWhenMapThenCreate() {
        when(companyMapper.mapFromDTOToEntity(Mockito.any(CompanyDTO.class)))
            .thenReturn(new Company());
        when(companyRepository.save(Mockito.any(Company.class)))
            .thenReturn(new Company());
        when(companyMapper.mapFromEntityToDTO(Mockito.any(Company.class)))
            .thenReturn(CompanyDTO.builder()
                .type("ADMIN").build());
        CompanyDTO actual = sut.create(CompanyDTO.builder()
            .type("ADMIN")
            .build());

        Assert.assertEquals("ADMIN", actual.getType());
    }

    @Test
    public void  testFindById() {
        when(companyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new Company()));
        when(companyMapper.mapFromEntityToDTO(Mockito.any(Company.class)))
            .thenReturn(new CompanyDTO());
        CompanyDTO actual = sut.findById("213");
        Assert.assertNotNull(actual);
    }

    @Test(expected = APIException.class)
    public void testGetCompanyInvalidId() {
        Company company = sut.getCompany("avc");
    }

    @Test(expected = APIException.class)
    public void testGetCompanyValidIdNullRepo() {
        when(companyRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        Company company = sut.getCompany("123");
    }
}
