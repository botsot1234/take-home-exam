package com.shipserv.takehomeexam.service;


import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.mapper.EmployeeMapper;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Employee;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Role;
import com.shipserv.takehomeexam.repository.EmployeeRepository;
import com.shipserv.takehomeexam.repository.RoleRepository;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RunWith(PowerMockRunner.class)
public class EmployeeServiceTest {

    @InjectMocks
    private EmployeeService sut;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @Mock
    private EmployeeMapper employeeMapper;

    @Mock
    private CompanyService companyService;

    @Test(expected = APIException.class)
    public void testRoleWhenNotExistingThenException() throws Exception {

        when(roleRepository.findByRole(anyString())).thenReturn(null);
        Whitebox.invokeMethod(sut,"getRole", "sad");

    }

    @Test
    public void testFindById() {
        when(employeeRepository.findById(anyLong())).thenReturn(Optional.of(new Employee()));
        when(employeeMapper.mapFromEntityToDTO(any(Employee.class)))
            .thenReturn(EmployeeDTO.builder()
            .id(Long.valueOf("123")).build());
        EmployeeDTO actual = sut.findById("123");
        assertEquals(Long.valueOf("123"), actual.getId());
    }

    @Test
    public void testSoftDelete() {
        when(employeeRepository.findById(anyLong())).thenReturn(Optional.of(new Employee()));
        when(employeeRepository.save(any(Employee.class))).thenReturn(Employee.builder().build());
        when(employeeMapper.mapFromEntityToDTO(any(Employee.class)))
            .thenReturn(EmployeeDTO.builder()
                .deleted(Flag.YES.getValue())
                .build());
        EmployeeDTO actual = sut.softDelete("123");
        assertEquals(Flag.YES.getValue(), actual.getDeleted());
    }

    @Test
    public void testUpdate() {
        when(companyService.getCompany(anyString())).thenReturn(Company.builder().build());
        when(employeeRepository.findById(anyLong())).thenReturn(Optional.of(new Employee()));
        when(roleRepository.findByRole(anyString())).thenReturn(Role.builder()
            .role("ADMIN")
            .build());

        when(employeeMapper.updateFromDTOToEntity(any(Employee.class),
            any(EmployeeDTO.class))).thenReturn(new Employee());
        when(employeeRepository.save(any(Employee.class)))
            .thenReturn(new Employee());
        when(employeeMapper.mapFromEntityToDTO(any(Employee.class)))
            .thenReturn(EmployeeDTO.builder()
                .id(Long.valueOf(123))
                .role("ADMIN").build());

        EmployeeDTO actual = sut.update(EmployeeDTO.builder()
        .id(Long.valueOf(123))
        .role("ADMIN").build());

        assertEquals("ADMIN", actual.getRole());
    }

    @Test
    public void testRoleWhenExistingThenReturnRole() throws Exception {

        when(roleRepository.findByRole(anyString())).thenReturn(Role.builder()
            .role("ADMIN")
            .build());
        Role role = Whitebox.invokeMethod(sut,"getRole", "sad");

        assertEquals("ADMIN", role.getRole());
    }

    @Test(expected = APIException.class)
    public void testGetEmployeeWhenInvalidParamsThenException() throws Exception {
        Whitebox.invokeMethod(sut, "getEmployee", "dasda");
    }

    @Test(expected = APIException.class)
    public void testGetEmployeeWhenValidParamsAndNotExistingEmployeeThenException() throws Exception {
        when(employeeRepository.findById(anyLong())).thenReturn(Optional.empty());
        Whitebox.invokeMethod(sut, "getEmployee", "123");
    }

    @Test
    public void testGetEmployeeWhenValidParamsAndExistingEmployeeThenReturnEmployee() throws Exception {
        when(employeeRepository.findById(anyLong())).thenReturn(Optional.of(new Employee()));
        Employee actual = Whitebox.invokeMethod(sut, "getEmployee", "123");
        Assert.assertNotNull(actual);
    }

    @Test(expected = APIException.class)
    public void testCreateWhenMapperThrowsIllegalArgExcThenException() {
        when(roleRepository.findByRole(anyString())).thenReturn(Role.builder()
            .role("ADMIN")
            .build());
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setCompanyId(Long.valueOf(1));
        employeeDTO.setRole("ADMIN");

        when(employeeMapper.mapFromDTOToEntity(any(EmployeeDTO.class)))
            .thenThrow(IllegalArgumentException.class);

        EmployeeDTO actual = sut.create(employeeDTO);
    }

    @Test
    public void testCreateWhenMapperIsOkThenSave() {
        when(roleRepository.findByRole(anyString())).thenReturn(Role.builder()
            .role("ADMIN")
            .build());
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setCompanyId(Long.valueOf(1));
        employeeDTO.setRole("ADMIN");

        when(employeeMapper.mapFromDTOToEntity(any(EmployeeDTO.class)))
            .thenReturn(new Employee());
        when(passwordEncoder.encode(any(CharSequence.class)))
            .thenReturn("123");
        when(employeeRepository.save(any(Employee.class)))
            .thenReturn(new Employee());
        when(employeeMapper.mapFromEntityToDTO(any(Employee.class)))
            .thenReturn(new EmployeeDTO());

        EmployeeDTO actual = sut.create(employeeDTO);

        Assert.assertNotNull(actual);
    }
}
