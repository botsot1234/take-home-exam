package com.shipserv.takehomeexam.service;

import static org.junit.Assert.assertNotNull;

import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceIntegTest {

    @Autowired
    private EmployeeService sut;

    @MockBean
    private EmployeeMapper employeeMapper;


    @Test
    public void testGetAll_WhenNullParams() {
        assertNotNull(sut.findAll(null, null, null, null, null));
    }

    @Test(expected = APIException.class)
    public void testGetAll_WhenNotNullParams() {
        assertNotNull(sut.findAll("any", "any", "any", "YES", "YES"));
    }


}
