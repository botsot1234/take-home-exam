package com.shipserv.takehomeexam.mapper;

import com.google.common.collect.Lists;
import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Employee;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Role;
import com.shipserv.takehomeexam.model.Type;
import com.shipserv.takehomeexam.repository.RoleRepository;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.util.CollectionUtils;

@RunWith(PowerMockRunner.class)
public class CompanyMapperTest {

    @InjectMocks
    private CompanyMapper sut;

    @Mock
    private EmployeeMapper employeeMapper;

    @Mock
    private RoleRepository roleRepository;


    @Test(expected = APIException.class)
    public void testRoleWhenNullThrowException() throws Exception {
        Mockito.when(roleRepository.findByRole(Mockito.anyString())).thenReturn(null);
        Role actual = Whitebox.invokeMethod(sut, "getRole", "ADMIN");
    }

    @Test
    public void testRoleWhenNotNullThenOK() throws Exception {
        Mockito.when(roleRepository.findByRole(Mockito.anyString())).thenReturn(new Role());
        Role actual = Whitebox.invokeMethod(sut, "getRole", "ADMIN");
        Assert.assertNotNull(actual);
    }

    @Test
    public void testMapFromDTOToEntity() {
        Mockito.when(employeeMapper.mapFromDTOToEntity(Mockito.any(EmployeeDTO.class)))
            .thenReturn(new Employee());
        Mockito.when(roleRepository.findByRole(Mockito.anyString())).thenReturn(new Role());

        Company actual = sut.mapFromDTOToEntity(CompanyDTO.builder()
            .type("BUYER")
            .address("add")
            .name("name")
            .employeeDTOList(Lists.newArrayList(EmployeeDTO.builder()
                .role("ADMIN")
                .build()))
            .build());

        Assert.assertTrue(!CollectionUtils.isEmpty(actual.getEmployees()));
    }

    @Test
    public void testMapFromExisting() {
        Mockito.when(roleRepository.findByRole(Mockito.anyString())).thenReturn(new Role());
        Mockito.when(employeeMapper.mapFromDTOToEntity(Mockito.any(EmployeeDTO.class)))
            .thenReturn(new Employee());

        Company actual = sut.mapFromExisting(Company.builder().employees(Lists.newArrayList()).build(), CompanyDTO.builder()
            .deleted("YES")
            .name("name")
            .address("add")
            .active("YES")
            .type("BUYER")
            .employeeDTOList(Lists.newArrayList(EmployeeDTO.builder()
                .role("ADMIN")
                .build()))
            .build());
        Assert.assertTrue(!CollectionUtils.isEmpty(actual.getEmployees()));
    }

    @Test
    public void testMapFromEntityToDTO() {

        Mockito.when(employeeMapper.mapFromListEntityToListDTO(Mockito.anyList()))
            .thenReturn(Lists.newArrayList(new EmployeeDTO()));

        CompanyDTO actual = sut.mapFromEntityToDTO(Company.builder()
            .id(Long.valueOf(1))
            .type(Type.BUYER)
            .active(Flag.YES)
            .deleted(Flag.YES)
            .address("add")
            .name("name")
            .employees(Lists.newArrayList(new Employee()))
            .build());
        Assert.assertTrue(!CollectionUtils.isEmpty(actual.getEmployeeDTOList()));
    }

    @Test
    public void testMapFromListEntityToListDTO() {
        List<CompanyDTO> actual = sut.mapFromListEntityToListDTO(Lists.newArrayList(Company.builder()
            .id(Long.valueOf(1))
            .type(Type.BUYER)
            .active(Flag.YES)
            .deleted(Flag.YES)
            .address("add")
            .name("name")
            .employees(Lists.newArrayList(new Employee()))
            .build()));
    }
}
