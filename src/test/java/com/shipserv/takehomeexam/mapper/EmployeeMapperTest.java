package com.shipserv.takehomeexam.mapper;

import com.google.common.collect.Lists;
import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Employee;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Role;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.CollectionUtils;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeMapperTest {

    @InjectMocks
    private EmployeeMapper sut;

    @Mock
    private CompanyMapper companyMapper;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    public void testMapFromEnitityToDTO() {
        Company company = Company.builder()
            .id(Long.valueOf(1))
            .name("name")
            .build();
        EmployeeDTO actual = sut.mapFromEntityToDTO(Employee.builder()
            .firstname("Daniel")
            .surname("Padilla")
            .username("Dj")
            .active(Flag.YES)
            .deleted(Flag.YES)
            .roles(new HashSet<>(Arrays.asList(new Role())))
            .company(company)
            .build());

        Assert.assertEquals("Dj", actual.getUsername());
    }

    @Test
    public void testMapFromDTOToEntity() {
        Employee actual = sut.mapFromDTOToEntity(EmployeeDTO.builder()
            .firstname("Daniel")
            .surname("Padilla")
            .username("Dj")
            .build());

        Assert.assertEquals("Dj", actual.getUsername());
    }

    @Test
    public void testMapFromListEntityToListDTO() {
        Company company = Company.builder()
            .id(Long.valueOf(1))
            .name("name")
            .build();
        Employee employee = Employee.builder()
            .firstname("Daniel")
            .surname("Padilla")
            .username("Dj")
            .active(Flag.YES)
            .deleted(Flag.YES)
            .roles(new HashSet<>(Arrays.asList(new Role())))
            .company(company)
            .build();
        List<EmployeeDTO> actual = sut.mapFromListEntityToListDTO(Lists.newArrayList(employee));

        Assert.assertTrue(!CollectionUtils.isEmpty(actual));
    }

    @Test
    public void testUpdateFromDTOToEntity() {
        Employee actual = sut.updateFromDTOToEntity(new Employee(), EmployeeDTO.builder()
            .password("password")
            .deleted("YES")
            .active("YES")
            .firstname("first")
            .surname("sur")
            .username("dj")
            .build());

        Assert.assertNotNull(actual);
    }
}
