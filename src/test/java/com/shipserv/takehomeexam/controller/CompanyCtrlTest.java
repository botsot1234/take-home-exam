package com.shipserv.takehomeexam.controller;


import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.service.CompanyService;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class CompanyCtrlTest {

    @InjectMocks
    private CompanyCtrl sut;

    @Mock
    private CompanyService companyService;

    private static final String ANY = "ANY";

    @Test
    public void testCreate() {
        when(companyService.create(any(CompanyDTO.class)))
            .thenReturn(new CompanyDTO());

        ResponseEntity<CompanyDTO> actual = sut.create(new CompanyDTO());

        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testFindById() {
        when(companyService.findById(anyString()))
            .thenReturn(new CompanyDTO());

        ResponseEntity<CompanyDTO> actual = sut.findById("123");

        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testFindAllExisting() {
        when(companyService.findAll(anyString(),
            anyString(), anyString(), anyString(),
            anyString())).thenReturn(Lists.newArrayList());

        ResponseEntity<List<CompanyDTO>> actual = sut.findAll(ANY, ANY, ANY, ANY, ANY);

        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testSoftDelete() {
        when(companyService.softDelete(anyString()))
            .thenReturn(new CompanyDTO());

        ResponseEntity<CompanyDTO> actual = sut.softDelete("123");

        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testUpdate() {
        when(companyService.update(any(CompanyDTO.class)))
            .thenReturn(new CompanyDTO());
        ResponseEntity<CompanyDTO> actual = sut.update("123", new CompanyDTO());
        assertEquals(HttpStatus.OK, actual.getStatusCode());

    }

}
