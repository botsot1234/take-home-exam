package com.shipserv.takehomeexam.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.service.EmployeeService;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeCtrlTest {

    @InjectMocks
    private EmployeeCtrl sut;

    @Mock
    private EmployeeService employeeService;

    private static final String ANY = "ANY";

    @Test
    public void testFindById() {

        when(employeeService.findById(anyString()))
            .thenReturn(new EmployeeDTO());

        ResponseEntity<EmployeeDTO> actual = sut.findById("123");

        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testCreate() {
        when(employeeService.create(any(EmployeeDTO.class)))
            .thenReturn(new EmployeeDTO());

        ResponseEntity<EmployeeDTO> actual = sut.create(new EmployeeDTO());

        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testFindAllExisting() {
        when(employeeService.findAll(anyString(), anyString(), anyString(), anyString(), anyString()))
            .thenReturn(Lists.newArrayList());
        ResponseEntity<List<EmployeeDTO>> actual = sut.findAll(ANY, ANY, ANY, ANY, ANY);
        assertEquals(HttpStatus.OK, actual.getStatusCode());

    }

    @Test
    public void testSoftDelete() {
        when(employeeService.softDelete(anyString()))
            .thenReturn(new EmployeeDTO());
        ResponseEntity<EmployeeDTO> actual = sut.softDelete("123");
        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }

    @Test
    public void testUpdate() {
        when(employeeService.update(any(EmployeeDTO.class)))
            .thenReturn(new EmployeeDTO());
        ResponseEntity<EmployeeDTO> actual = sut.update("123", new EmployeeDTO());
        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }
}
