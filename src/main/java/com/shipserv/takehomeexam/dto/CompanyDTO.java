package com.shipserv.takehomeexam.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@RequiredArgsConstructor
@Getter
@AllArgsConstructor(onConstructor = @__(@JsonIgnore))
@JsonInclude(Include.NON_NULL)
public class CompanyDTO implements Serializable {

    private static final long serialVersionUID = 7039641676077489746L;
    private Long id;
    private String name;

    private String address;
    private String type;
    private String active;
    private String deleted;

    private List<EmployeeDTO> employeeDTOList;
}
