package com.shipserv.takehomeexam.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@RequiredArgsConstructor
@Getter
@AllArgsConstructor(onConstructor = @__(@JsonIgnore))
@JsonInclude(Include.NON_NULL)
public class EmployeeDTO {
    private Long id;

    private String firstname;
    private String surname;
    private String username;
    private String password;
    private String active;
    private String deleted;
    private String role;
    private Long companyId;
    private String companyName;
}
