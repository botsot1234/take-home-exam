package com.shipserv.takehomeexam.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;
    private static final String[] AUTH_LIST = {
        "/v2/api-docs",
        "/configuration/ui",
        "/swagger-resources/**",
        "/configuration/security",
        "/swagger-ui.html",
        "/webjars/**"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()

            .antMatchers("/console/**").permitAll()
            .antMatchers(AUTH_LIST).authenticated()

            .antMatchers("/employee/**").hasAnyAuthority("USER", "ADMIN")
            .antMatchers("/company/**").hasAuthority("ADMIN")
            .antMatchers("/company/list").hasAnyAuthority("ADMIN", "USER")
            .anyRequest()
            .authenticated()
            .and().headers().frameOptions().sameOrigin()
            .and().httpBasic()
            .authenticationEntryPoint(authenticationEntryPoint);
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authentication)
        throws Exception {
        authentication.inMemoryAuthentication()
            .withUser("DJ").password(passwordEncoder().encode("PASSWORD")).authorities("ADMIN")
            .and()
            .withUser("JR").password(passwordEncoder().encode("PASSWORD")).authorities("USER");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
