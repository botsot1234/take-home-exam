package com.shipserv.takehomeexam.repository;

import com.shipserv.takehomeexam.model.Employee;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByUsername(String username);

    List<Employee> findAll(Specification<Employee> specification);
}
