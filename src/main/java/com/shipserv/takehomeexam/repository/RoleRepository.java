package com.shipserv.takehomeexam.repository;

import com.shipserv.takehomeexam.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRole(String role);
}
