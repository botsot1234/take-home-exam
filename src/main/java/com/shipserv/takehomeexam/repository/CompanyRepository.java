package com.shipserv.takehomeexam.repository;

import com.shipserv.takehomeexam.model.Company;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    /**
     * This query will find all company by deleted and flag
     */
    List<Company> findAll(Specification<Company> specification);
}
