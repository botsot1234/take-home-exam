package com.shipserv.takehomeexam.mapper;

import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Employee;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Role;
import com.shipserv.takehomeexam.model.Type;
import com.shipserv.takehomeexam.repository.RoleRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class CompanyMapper {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private RoleRepository roleRepository;


    private Role getRole(String role) {
        return Optional.ofNullable(roleRepository.findByRole(role))
            .orElseThrow(() -> new APIException(HttpStatus.NOT_FOUND, "Role not found"));
    }


    /**
     * @return Company
     */
    public Company mapFromDTOToEntity(CompanyDTO companyDTO) throws IllegalArgumentException {
        Company company = Company.builder()
            .type(Type.valueOf(companyDTO.getType()))
            .active(Flag.YES)
            .deleted(Flag.NO)
            .address(companyDTO.getAddress())
            .name(companyDTO.getName())
            .build();

        List<Employee> employees = new ArrayList<>();
        companyDTO.getEmployeeDTOList().forEach(employeeDTO -> {
            Employee employee = employeeMapper.mapFromDTOToEntity(employeeDTO);

            Role role = getRole(employeeDTO.getRole());
            employee.setRoles(new HashSet<>(Arrays.asList(role)));

            employee.setCompany(company);
            employees.add(employee);
        });

        company.setEmployees(employees);

        return company;
    }

    public Company mapFromExisting(Company existing, CompanyDTO companyDTO) {
        existing.setDeleted(Flag.valueOf(companyDTO.getDeleted()));
        existing.setName(companyDTO.getName());
        existing.setAddress(companyDTO.getAddress());
        existing.setActive(Flag.valueOf(companyDTO.getActive()));
        existing.setType(Type.valueOf(companyDTO.getType()));

        List<Employee> employees = new ArrayList<>();

        companyDTO.getEmployeeDTOList().forEach(employeeDTO -> {
            Employee employee = employeeMapper.mapFromDTOToEntity(employeeDTO);

            Role role = getRole(employeeDTO.getRole());
            employee.setRoles(new HashSet<>(Arrays.asList(role)));

            employee.setCompany(existing);
            employees.add(employee);
        });
        // if there are existing employees remove all then map again
        if (!CollectionUtils.isEmpty(existing.getEmployees())) {
            existing.getEmployees().clear();
            existing.getEmployees().addAll(employees);
        } else {
            existing.setEmployees(employees);
        }


        return existing;
    }

    public CompanyDTO mapFromEntityToDTO(Company company) {
        CompanyDTO companyDTO = CompanyDTO.builder()
            .id(company.getId())
            .type(company.getType().getValue())
            .active(company.getActive().getValue())
            .deleted(company.getDeleted().getValue())
            .type(company.getType().getValue())
            .address(company.getAddress())
            .name(company.getName())
            .build();

        companyDTO.setEmployeeDTOList(employeeMapper.mapFromListEntityToListDTO(company.getEmployees()));

        return companyDTO;
    }

    public List<CompanyDTO> mapFromListEntityToListDTO(List<Company> companies) {
        List<CompanyDTO> companyDTOS = new ArrayList<>();
        companies.forEach(company -> companyDTOS.add(mapFromEntityToDTO(company)));
        return companyDTOS;
    }
}
