package com.shipserv.takehomeexam.mapper;

import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.model.Employee;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Role;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper {


    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public EmployeeDTO mapFromEntityToDTO(Employee employee) {
        Role role = employee.getRoles().stream().findFirst().orElse(new Role());
        return EmployeeDTO.builder()
            .firstname(employee.getFirstname())
            .surname(employee.getSurname())
            .username(employee.getUsername())
            .active(employee.getActive().getValue())
            .deleted(employee.getDeleted().getValue())
            .role(role.getRole())
            .companyId(employee.getCompany().getId())
            .companyName(employee.getCompany().getName())
            .build();
    }

    /**
     * for 1st time create only
     * @param employeeDTO
     * @return
     */
    public Employee mapFromDTOToEntity(EmployeeDTO employeeDTO) {
        return Employee.builder()
            .firstname(employeeDTO.getFirstname())
            .surname(employeeDTO.getSurname())
            .username(employeeDTO.getUsername())
            .active(Flag.YES)
            .deleted(Flag.NO)
            .build();
    }

    public List<EmployeeDTO> mapFromListEntityToListDTO(List<Employee> employees) {
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        employees.forEach(employee -> employeeDTOS.add(mapFromEntityToDTO(employee)));
        return employeeDTOS;
    }


    public Employee updateFromDTOToEntity(Employee existing, EmployeeDTO payload) {
        existing.setDeleted(Flag.valueOf(payload.getDeleted()));
        existing.setActive(Flag.valueOf(payload.getActive()));
        existing.setFirstname(payload.getFirstname());
        existing.setSurname(payload.getSurname());
        existing.setUsername(payload.getUsername());
        existing.setPassword(passwordEncoder.encode(payload.getPassword()));

        return existing;
    }
}
