package com.shipserv.takehomeexam.controller;

import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.exception.ApiErrorResponse;
import com.shipserv.takehomeexam.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
@Api(value = "Employee controller")
public class EmployeeCtrl {

    @Autowired
    private EmployeeService employeeService;


    @ApiOperation(value = "Create employee", response = CompanyDTO.class)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeDTO> create(@RequestBody EmployeeDTO employeeDTO) {
        return ResponseEntity.ok(employeeService.create(employeeDTO));
    }

    @ApiOperation(value = "Find employee", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeDTO> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(employeeService.findById(id));
    }

    @ApiOperation(value = "Find all employee", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @GetMapping
    public ResponseEntity<List<EmployeeDTO>> findAll(@RequestParam(value = "firstname", required = false) String firstname,
        @RequestParam(value = "surname", required = false) String surname, @RequestParam(value = "username", required = false) String username,
        @RequestParam(value = "active", required = false) String active, @RequestParam(value = "deleted", required = false) String deleted) {
        return ResponseEntity.ok(employeeService.findAll(firstname, surname, username, active, deleted));
    }

    @ApiOperation(value = "Delete employee", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeDTO> softDelete(@PathVariable("id") String id) {
        return ResponseEntity.ok(employeeService.softDelete(id));
    }


    @ApiOperation(value = "Update company", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeeDTO> update(@PathVariable("id") String id, @RequestBody EmployeeDTO employeeDTO) {
        employeeDTO.setId(Long.valueOf(id));
        return ResponseEntity.ok(employeeService.update(employeeDTO));
    }
}
