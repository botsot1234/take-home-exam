package com.shipserv.takehomeexam.controller;

import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.exception.ApiErrorResponse;
import com.shipserv.takehomeexam.service.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
@Api(value = "Company controller")
public class CompanyCtrl {

    @Autowired
    CompanyService companyService;


    @ApiOperation(value = "Create company", response = CompanyDTO.class)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDTO> create(@RequestBody CompanyDTO companyDTO) {
        return ResponseEntity.ok(companyService.create(companyDTO));
    }

    @ApiOperation(value = "Find company", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDTO> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(companyService.findById(id));
    }

    @ApiOperation(value = "Find all company", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @GetMapping(value = "/list")
    public ResponseEntity<List<CompanyDTO>> findAll(@RequestParam(value = "name", required = false) String name,
        @RequestParam(value = "address", required = false) String address,
        @RequestParam(value = "type", required = false) String type,
        @RequestParam(value = "active", required = false) String active,
        @RequestParam(value = "deleted", required = false) String deleted) {
        return ResponseEntity.ok(companyService.findAll(name, address, type, active, deleted));
    }

    @ApiOperation(value = "Delete company", response = CompanyDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful"),
        @ApiResponse(code = 404, message = "Not found", response = ApiErrorResponse.class)
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDTO> softDelete(@PathVariable("id") String id) {
        return ResponseEntity.ok(companyService.softDelete(id));
    }


    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CompanyDTO> update(@PathVariable("id") String id, @RequestBody CompanyDTO companyDTO) {
        companyDTO.setId(Long.valueOf(id));
        return ResponseEntity.ok(companyService.update(companyDTO));
    }
}
