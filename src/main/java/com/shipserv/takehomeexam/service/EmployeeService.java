package com.shipserv.takehomeexam.service;

import com.shipserv.takehomeexam.dto.EmployeeDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.mapper.EmployeeMapper;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Employee;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Role;
import com.shipserv.takehomeexam.repository.EmployeeRepository;
import com.shipserv.takehomeexam.repository.RoleRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private CompanyService companyService;

    public EmployeeDTO softDelete(String id) {
        Employee toDelete = getEmployee(id);
        toDelete.setDeleted(Flag.YES);
        toDelete = employeeRepository.save(toDelete);
        return employeeMapper.mapFromEntityToDTO(toDelete);
    }


    public EmployeeDTO findById(String id) {
        return employeeMapper.mapFromEntityToDTO(getEmployee(id));
    }

    public EmployeeDTO update(EmployeeDTO employeeDTO) {
        Company company = companyService.getCompany(String.valueOf(employeeDTO.getCompanyId()));
        Role role = getRole(employeeDTO.getRole());
        Employee toUpdate = getEmployee(String.valueOf(employeeDTO.getId()));
        toUpdate.setCompany(company);
        toUpdate.setRoles(new HashSet<>(Arrays.asList(role)));
        toUpdate = employeeMapper.updateFromDTOToEntity(toUpdate, employeeDTO);
        toUpdate = employeeRepository.save(toUpdate);
        return employeeMapper.mapFromEntityToDTO(toUpdate);
    }

    public EmployeeDTO create(EmployeeDTO employeeDTO) {
        Company company = companyService.getCompany(String.valueOf(employeeDTO.getCompanyId()));
        Role role = getRole(employeeDTO.getRole());
        Employee toSave;
        try {
            toSave = employeeMapper.mapFromDTOToEntity(employeeDTO);
            toSave.setPassword(passwordEncoder.encode(employeeDTO.getPassword()));
            toSave.setRoles(new HashSet<>(Arrays.asList(role)));
            toSave.setCompany(company);
        } catch (IllegalArgumentException iex) {
            log.error(iex.getMessage());
            throw new APIException(HttpStatus.BAD_REQUEST, "Invalid Payload");
        }

        toSave = employeeRepository.save(toSave);

        return employeeMapper.mapFromEntityToDTO(toSave);
    }

    public List<EmployeeDTO> findAll(String firstname, String surname, String username,
        String active, String deleted) {
        List<Employee> employees = employeeRepository.findAll(new Specification<Employee>() {
            @Nullable
            @Override
            public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> criteriaQuery,
                CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();

                if (StringUtils.isNotBlank(firstname)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("firstname"), firstname)));
                }

                if (StringUtils.isNotBlank(surname)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("surname"), surname)));
                }

                if (StringUtils.isNotBlank(username)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("username"), username)));
                }

                if (StringUtils.isNotBlank(active)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("active"), Flag.valueOf(active.toUpperCase()))));
                }

                if (StringUtils.isNotBlank(deleted)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("deleted"), Flag.valueOf(deleted.toUpperCase()))));
                }


                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });
        if (CollectionUtils.isEmpty(employees)) {
            throw new APIException(HttpStatus.NOT_FOUND, "No existing employees");
        }
        return employeeMapper.mapFromListEntityToListDTO(employees);
    }

    private Employee getEmployee(String id) {
        Long employeeId;
        try {
            employeeId = Long.valueOf(id);
        } catch (NumberFormatException n) {
            throw new APIException(HttpStatus.BAD_REQUEST, "Invalid id parameter");
        }
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (!employee.isPresent()) {
            String err = StringUtils.join("Employee with id: ", id, " is not existing");
            throw new APIException(HttpStatus.NOT_FOUND, err);
        }

        return employee.get();
    }


    private Role getRole(String role) {
        return Optional.ofNullable(roleRepository.findByRole(role))
            .orElseThrow(() -> new APIException(HttpStatus.NOT_FOUND, "Role not found"));
    }



}
