package com.shipserv.takehomeexam.service;

import com.shipserv.takehomeexam.dto.CompanyDTO;
import com.shipserv.takehomeexam.exception.APIException;
import com.shipserv.takehomeexam.mapper.CompanyMapper;
import com.shipserv.takehomeexam.model.Company;
import com.shipserv.takehomeexam.model.Flag;
import com.shipserv.takehomeexam.model.Type;
import com.shipserv.takehomeexam.repository.CompanyRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;



    @Autowired
    private CompanyMapper companyMapper;

    public CompanyDTO softDelete(String id) {
        Company toDelete = getCompany(id);
        toDelete.setDeleted(Flag.YES);
        toDelete = companyRepository.save(toDelete);
        return companyMapper.mapFromEntityToDTO(toDelete);
    }


    public CompanyDTO update(CompanyDTO companyDTO) {
        Company existing = getCompany(String.valueOf(companyDTO.getId()));
        existing = companyMapper.mapFromExisting(existing, companyDTO);
        existing = companyRepository.save(existing);
        return companyMapper.mapFromEntityToDTO(existing);
    }


    public CompanyDTO create(CompanyDTO companyDTO) throws APIException {
        Company toSave;
        try {
            toSave = companyMapper.mapFromDTOToEntity(companyDTO);
        } catch (IllegalArgumentException iex) {
            log.error(iex.getMessage());
            throw new APIException(HttpStatus.BAD_REQUEST, "Invalid Payload");
        }

        toSave = companyRepository.save(toSave);

        return companyMapper.mapFromEntityToDTO(toSave);
    }


    public CompanyDTO findById(String id) throws APIException {
        return companyMapper.mapFromEntityToDTO(getCompany(id));
    }

    public List<CompanyDTO> findAll(String name, String address, String type,
        String active, String deleted) throws APIException {
        List<Company> companies = companyRepository.findAll(new Specification<Company>() {
            @Nullable
            @Override
            public Predicate toPredicate(Root<Company> root, CriteriaQuery<?> criteriaQuery,
                CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();

                if (StringUtils.isNotBlank(name)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("name"), name)));
                }

                if (StringUtils.isNotBlank(address)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("address"), address)));
                }

                if (StringUtils.isNotBlank(type)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("type"),
                        Type.valueOf(type.toUpperCase()))));
                }

                if (StringUtils.isNotBlank(active)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("active"), Flag.valueOf(active.toUpperCase()))));
                }

                if (StringUtils.isNotBlank(deleted)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("deleted"), Flag.valueOf(deleted.toUpperCase()))));
                }

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        });
        if (CollectionUtils.isEmpty(companies)) {
            throw new APIException(HttpStatus.NOT_FOUND, "No existing companies");
        }

        return companyMapper.mapFromListEntityToListDTO(companies);
    }

    public Company getCompany(String id) {
        Long companyId;

        try {
            companyId = Long.valueOf(id);
        } catch (NumberFormatException n) {
            throw new APIException(HttpStatus.BAD_REQUEST, "Invalid id parameter");
        }

        Optional<Company> company = companyRepository.findById(companyId);
        if (!company.isPresent()) {
            String err = StringUtils.join("Company with id: ", id, " is not existing");
            throw new APIException(HttpStatus.NOT_FOUND, err);
        }
        return company.get();
    }
}
