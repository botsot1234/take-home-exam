package com.shipserv.takehomeexam.model;

public enum Type {

    BUYER("BUYER"),
    SUPPLIER("SUPPLIER");

    private String value;

    Type(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
