package com.shipserv.takehomeexam.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Builder
@AllArgsConstructor(onConstructor = @__(@JsonIgnore))
@Getter
@RequiredArgsConstructor
@Data
@Entity
@Table(name = "company")
public class Company implements Serializable {


    private static final long serialVersionUID = 2367718653189612141L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String address;

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private Type type;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    private Flag active;

    @Column(length = 3)
    @Enumerated(EnumType.STRING)
    private Flag deleted;

    @OneToMany(mappedBy = "company", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Employee> employees;
}
