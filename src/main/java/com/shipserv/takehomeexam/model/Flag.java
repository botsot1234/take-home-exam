package com.shipserv.takehomeexam.model;

public enum Flag {

    YES("YES"),
    NO("NO");

    private String value;

    Flag(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
