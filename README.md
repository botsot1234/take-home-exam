# ShipServ Takehome Exam

## Prerequisites ##
1. Java 8
2. Maven
3. H2 Database (No need to install since it is embedded in memory)
4. Postman
5. IntelliJ/Eclipse
6. Lombok plugin support

## Installation ##
1. Go to project and invoke ``mvn clean install``
2. Import in Eclipse/Intellij as maven project and make sure it supports Lombok plugin
3. Invoke ``mvn spring-boot:run``
4. Access ``http://localhost:8080/shipserv-exam/swagger-ui.html`` if username and password prompt used this credentials
      1. username: DJ, password: PASSWORD, role: ADMIN
      2. username: JR, password: PASSWORD, role: USER
5. For additional api request reference import ``shipserv.postman_collection.json`` in your postman
6. Make sure in the postman request you supply basic authentication in the headers
7. Access ``http://localhost:8080/shipserv-exam/h2-console`` to check h2 console. Here are the credentials:
      1. Driver: ``org.h2.Driver``, jdbcUrl: ``jdbc:h2:mem:testdb``, username: ``sa``, password: ``passsword``

## Challenges ##
1. Did not complete Spring Security requirements regarding role base access to specific API's but in order
to replicate the requirement I implement in memory authentication which provides default username and password with
role.
2. Cannot inject basic authentication in every API's in swagger ui to test you must access it in postman. Provided by this user references:
      1. username: DJ, password: PASSWORD, role: ADMIN
      2. username: JR, password: PASSWORD, role: USER
3. Did not manage to finish unit test, needs more time
      

